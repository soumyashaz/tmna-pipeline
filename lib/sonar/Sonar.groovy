HOST = "http://localhost:9000/"

def scan(String propertiesFile, String projectRoot)
{
  def scanstatus
  def scannerHome = tool 'SonarScanner'
    
	withSonarQubeEnv('Sonarqube') 
  {        
    scanstatus = sh returnStatus: true, script: "${scannerHome} -Dsonar.projectBaseDir=\"${projectRoot}\" -Dproject.settings=${propertiesFile} -Dsonar.host.url=${HOST}" 
    sh "echo 'Scan status is: ${scanstatus}'"
  }
  timeout(time: 30, unit: 'MINUTES')
  { 
    // Just in case something goes wrong, pipeline will be killed after a timeout
    if(scanstatus==0)
    {  
      sh 'sleep 30' 
      def qg = waitForQualityGate() 
      sh "echo 'QualityGate status is: ${qg.status}'"      
      if (qg.status == 'ERROR')
      {
        currentBuild.result = 'FAILURE'
        error "Pipeline aborted due to quality gate failure: ${qg.status}"
      }
      else
      {
        currentBuild.result = 'SUCCESS'
      }      
    }
    else
    { 
      currentBuild.result = 'FAILURE'
    }
  }
    return currentBuild.result  
}
return this