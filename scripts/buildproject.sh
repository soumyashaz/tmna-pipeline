mkdir tmp || true
rm -rf tmp/* || true
cat 'project-manifest.txt' | xargs -d $'\n' --verbose -a 'project-manifest.txt'  cp --parents -r -t 'tmp' || true
cp -R tmp/force-app/main/default/* force-app/main/default/
rm -rfv tmp || true
find force-app/main/default